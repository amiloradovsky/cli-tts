#!/bin/sh

[ -z "$VOICE" ] && VOICE=slt
[ -z "$AUDIO" ] && AUDIO=$(mktemp /tmp/audio-XXXX)
[ -z "$TEXT" ] && TEXT=$(mktemp -u /tmp/text-XXXX)

mkfifo "$TEXT"

(xsel -s; xsel -p) | tr '\\' ' ' \
    | sed -e 's/^\s*//' -e 's/\s*$//' -e "$SC" \
    | sed -E -z 's/^\n*/\n/; s/\n*$/\n/; s/\n\n+/\n\n/g' \
    | tee "$TEXT" &
if which mimic >/dev/null
then mimic -voice "$VOICE" "$TEXT" "$AUDIO" && paplay "$AUDIO"
elif which festival >/dev/null
then festival --tts < "$TEXT"
else echo "No TTS engine found." >&2 ; exit 127
fi

rm "$AUDIO" "$TEXT"
