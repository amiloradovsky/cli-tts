#!/bin/sh

reset_sc ()
{
    export SC=''
}

set_sc ()
{
    export SC='G'
}

reset_prompt ()
{
    echo -n | xsel -i -s
    P=$(echo -e "\rx")
}

set_prompt ()
{
    xsel -x
    P=$(echo -e "\rX")
}

reset_all ()
{
    reset_sc
    reset_prompt
}

echo "Press <Esc> to exit."
echo "Press + to enqueue beginning of paragraph."
echo "Press <Space> to insert pauses between lines."
echo "Press any other key to say the selection aloud."

reset_all
while read -s -p "$P" -N 1; do
    case "$REPLY" in
        " ") [ -z "$SC" ] && set_sc || reset_sc ;;
        "+") set_prompt ;;
        ""|"") echo -e "\r"; break ;;
        *) $(dirname "$0")/say.sh
           reset_all
    esac
done
